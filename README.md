# pagekite-mbed-test
> Simple test program to show the [Pagekite mbed](https://gitlab.com/essensium-mind/pagekite-mbed) backend integration.

It has been tested with mbed-cli on an LPC1768 mbed platform.

## Installation
First, install the mbed-cli:

    pip install mbed-cli

To initialize this repository, clone it using `mbed import`:

    mbed import git@gitlab.com:essensium-mind/pagekite-mbed-test.git pagekite-mbed-test

This will clone the repository as well as its sub-repositories, and set
up mbed-cli for it. Next, set the toolchain and target:

    mbed target LPC1768
    mbed toolchain GCC_ARM

## Building
To build using mbed-cli, make sure the compiler `arm-none-eabi-g++` is
in your PATH, and run `mbed compile`.

Publishing changes is OK using normal git as long as the MBED libraries
in use are not changed. If you change the MBED libraries, use "mbed
publish" to commit and push those changes.

Build with

    mbed compile

## Running
Install with

    pmount sdb
    cp BUILD/LPC1768/GCC_ARM/*.bin /media/sdb
    pumount sdb

Then press the reset button.

To watch the serial output of the prototype

    microcom -s 9600 -p /dev/ttyACM0

To test your (raw) kite:

Modify the KITENAME, PUBLIC_PORT and SECRET in main.cpp to match your kite frontend. You can create a frontend on pagekite.me, or you can start a local one with:
```
pagekite --clean --isfrontend --ports=80,2222 --rawports=virtual --protos=http,raw --domain=http,raw:10.3.4.128:mysecret
```
Note that if you can't create the frontend on port 80, you need to modify PagekiteBackend.cpp and set _frontend_port to your frontend's port.

```
nc -X connect -x KITENAME:PUBLICPORT KITENAME PUBLICPORT
```

## Meta
[Essensium - Mind](https://www.mind.be/)

* Bert Outtier
* Arnout Vandecappelle

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/essensium-mind/pagekite-mbed](https://gitlab.com/essensium-mind/pagekite-mbed)

## Contributing

1. Fork it (<https://gitlab.com/yourname/yourproject/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
