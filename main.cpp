/*
 * PagekiteConnection.cpp
 *
 * Copyright 2017 Arnout Vandecappelle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included
 *   in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */


#include <mbed.h>
#include <mbedtls/entropy.h>
#include "pagekite/PagekiteBackend.h"
#include <EthernetInterface.h>

#define KITENAME "backendtestbert.pagekite.me"
#define PUBLIC_PORT 2222
#define SECRET "mysecret"

static DigitalOut led1(LED1);

// Use 4 analog input as seed for the rng
static AnalogIn analogpins[] = {p15, p16, p17, p18};

static PagekiteBackend pk (KITENAME, PUBLIC_PORT, SECRET);

// main() runs in its own thread in the OS
int main()
{
    printf("Starting network...\r\n");
    EthernetInterface eth;
    eth.set_dhcp(true);
    eth.connect();

    const char *ip = eth.get_ip_address();
    printf("IP address: %s\r\n", ip ? ip : "No IP");
    const char *mac = eth.get_mac_address();
    printf("MAC address: %s\r\n", mac ? mac : "No MAC");

    printf("Connecting to %s... ", KITENAME);
    nsapi_error_t err = pk.connect(&eth);
    if(err != NSAPI_ERROR_OK) {
        printf("Connection failed! %d\r\n", err);
    }
    else {
        printf("Connection success!\r\n");
        wait(.1);

        PagekiteConnection *connection;
        while(true) {
            err = pk.accept(connection);
            if(err != NSAPI_ERROR_OK) {
                printf("Error accepting new connection: %d\r\n", err);
            }
            else {
                printf("New connection with stream ID %ld opened!\n", connection->getStreamID());
                static char buf[300];
                err = connection->recv(buf, 300);
                if(err < 0) {
                    printf("Error receiving from connection: %d\n", err);
                }
                else {
                    printf("Received: %.*s\nReplying...\n", err, buf);
                    if (err + 7 < sizeof(buf) - 1)
                    {
                        strcpy(&buf[err], " reply\n");
                        err += 7;
                    }
                    connection->send(buf, err);
                    printf("Closing connection with stream ID %ld again\n", connection->getStreamID());
                    connection->close();
                }
            }
        }
    }

    while (true) {
        led1 = !led1;
        wait(.1);
    }
}

extern "C" {

/* Used by mbedtls which is C code */
int mbedtls_hardware_poll(void *data, unsigned char *output, size_t len, size_t *olen ) {
    uint16_t value;
    char *buffer = (char *)output;

    for (size_t i = 0; i < len; ++i) {
        buffer[i] = 0;
        for (size_t bit = 0; bit < 8; bit++) {
            buffer[i] = buffer[i]<<1;
            value = analogpins[bit%4].read_u16(); // reads a 10 bit ADC normalised to 16 bits.
            if (value & 0x0040)          // LSB of ADC output is a 1
                buffer[i]++;
        }
    }
    *olen = len;

    return 0;
}

} // extern "C"
